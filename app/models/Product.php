<?php

namespace App\Models;

use Core\Model;
use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class Product extends Model
{

    //Predefine Here
    public $id;
    public $name;
    public $description;
    public $user_id;
    public $image;
    public $user = Null;


    public function user()
    {
        if (isset($this->user)) {
            return $this->user;
        }

        $stmt = $this->db->prepare("SELECT
            users.*
        FROM
            users
        LEFT JOIN products ON products.user_id = users.id
        WHERE products.id = ?
        ");
        $result = $stmt->execute([$this->id]);

        return $this->user = $stmt->fetchAll(PDO::FETCH_CLASS, 'App\Models\User')[0];

    }
}
