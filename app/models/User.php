<?php

namespace App\Models;

use Core\Model;
use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class User extends Model
{

    //Predefine Here
    public $id;
    public $first_name;
    public $last_name;
    public $email;
    public $bio;
    protected $roles = Null;

    public function fullname()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function roles()
    {
        if (isset($this->roles)) {
            return $this->roles;
        }

        $stmt = $this->db->prepare("SELECT
            roles.*
        FROM
            roles
        LEFT JOIN roles_users ON roles_users.role_id = roles.id
        WHERE user_id = ?
        ");
        $result = $stmt->execute([$this->id]);

        return $this->roles = $stmt->fetchAll(PDO::FETCH_CLASS, 'App\Models\Role');

    }

    public function hasRole(Array $roles){
        foreach ($this->roles() as $role){
            foreach ($roles as $r){
                if(strtolower($role->name) === strtolower($r)){
                    return true;
                }
            }
        }
        return false;
    }
}
