<?php

namespace App\Repositories;

use App\Models\Product;
use Core\Repository;
use App\Models\User;
use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class Users extends Repository
{

    /**
     * Get a the user
     *
     * @return array
     */
    public static function get(int $id)
    {

        $db = static::getDB();
        $stmt = $db->prepare("SELECT * FROM users WHERE id= ? ");
        $result = $stmt->execute([$id]);

        $users = $stmt->fetchAll(PDO::FETCH_CLASS, 'App\Models\User');

        if (empty($users)) {
            return False;
        }

        return $users[0];
    }

    /**
     * Get all the users
     *
     * @return array
     */
    public static function getAll()
    {
        $db = static::getDB();
        $stmt = $db->query('SELECT * FROM users');
        return $stmt->fetchAll(PDO::FETCH_CLASS, 'App\Models\User');
    }

    /**
     * Get all the users
     *
     * @return array
     */
    public static function getUserForLogin(String $email, String $password)
    {
        $db = static::getDB();
        $stmt = $db->prepare("SELECT * FROM users WHERE email= ? ");
        $result = $stmt->execute([$email]);

        if ($result) {
            $users = $stmt->fetchAll(PDO::FETCH_CLASS, 'App\Models\User');
            if($users && password_verify($password, $users[0]->password)){
                return $users[0];
            }
        }

        return False;
    }

    /**
     * Create User
     *
     * @param User $user
     * @return array
     */
    public static function create(User $user)
    {
        $db = static::getDB();
        $stmt = $db->prepare("INSERT INTO
                users (first_name, last_name, email, password) 
            VALUES
                (?, ?, ?, ?);
        ");
        try {
            $result = $stmt->execute([
                $user->first_name,
                $user->last_name,
                $user->email,
                password_hash($user->password, PASSWORD_DEFAULT),
            ]);

            return static::get($db->lastInsertId());
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function edit(User $user)
    {
        $db = static::getDB();
        if($user->password) {
            $stmt = $db->prepare("UPDATE users
                SET
                    first_name = ?,
                    last_name = ?,
                    email = ?,
                    password = ?
                WHERE
                    users.id = ?
            ");
            try {
                return $stmt->execute([
                    $user->first_name,
                    $user->last_name,
                    $user->email,
                    password_hash($user->password, PASSWORD_DEFAULT),
                    $user->id,
                ]);

            } catch (\Exception $e) {
                return false;
            }
        }else{
            $stmt = $db->prepare("UPDATE users
                SET
                    first_name = ?,
                    last_name = ?,
                    email = ?
                WHERE
                    users.id = ?
            ");
            try {
                return $stmt->execute([
                    $user->first_name,
                    $user->last_name,
                    $user->email,
                    $user->id,
                ]);

            } catch (\Exception $e) {
                return false;
            }
        }
    }
}
