<?php

namespace App\Repositories;

use Core\Repository;
use App\Models\User;
use App\Models\Product;
use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class Products extends Repository
{

    /**
     * Get all the users
     *
     * @return array
     */
    public static function getAll(int $page = Null, int $page_size = 10)
    {
        $db = static::getDB();
        $sql = "SELECT * FROM products";
        if (isset($page)) {
            $offset = $page_size * $page;
            $sql .= "LIMIT ? OFFSET ?";
        }
        $stmt = $db->query($sql);
        $result = $stmt->execute([$page_size, $page]);
        return $stmt->fetchAll(PDO::FETCH_CLASS, 'App\Models\Product');
    }

    /**
     * Create User
     *
     * @param Product $product
     * @return array
     */
    public static function create(Product $product)
    {
        $db = static::getDB();
        $stmt = $db->prepare("INSERT INTO
                products (name, description, user_id, image) 
            VALUES
                (?, ?, ?, ?);
        ");
        try {
            $result = $stmt->execute([
                $product->name,
                $product->description,
                $product->user_id,
                $product->image,
            ]);

            return static::get($db->lastInsertId());
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Get a the user
     *
     * @return array
     */
    public static function get(int $id)
    {
        $db = static::getDB();
        $stmt = $db->prepare("SELECT * FROM products WHERE id= ? ");
        $result = $stmt->execute([$id]);

        $products = $stmt->fetchAll(PDO::FETCH_CLASS, 'App\Models\Product');
        if (empty($products)) {
            return False;
        } else {
            return $products[0];
        }
    }

    public static function edit(Product $product)
    {
        $db = static::getDB();
        $stmt = $db->prepare("UPDATE products
            SET
                name = ?,
                description = ?,
                user_id = ?,
                image = ?
            WHERE
                products.id = ?
        ");
        try {
            return $stmt->execute([
                $product->name,
                $product->description,
                $product->user_id,
                $product->image,
                $product->id,
            ]);

        } catch (\Exception $e) {
            return false;
        }
    }
}
