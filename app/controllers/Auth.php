<?php


namespace App\controllers;


use App\Models\User;
use App\Repositories\Users;
use Core\Controller;
use Core\Response;
use Core\Auth as A;
use Exception;

class Auth extends Controller
{

    /**
     * Register User
     *
     * @return void
     * @throws Exception
     */
    public function register()
    {
        $user = new User();
        $user->first_name   = $_POST['first_name'] ?? '';
        $user->last_name    = $_POST['last_name'] ?? '';
        $user->email        = $_POST['email'] ?? '';
        $user->password     = $_POST['password'] ?? '';

        $new_user = Users::create($user);

        if($new_user) {
//            A::login_user($new_user);
            Response::json([
                'message' => 'Register was successful.'
            ]);
        }else{
            Response::json([
                'message' => 'Something went wrong.'
            ]);
        }

        Response::back();
    }

    /**
     * Login User
     *
     * @return void
     * @throws Exception
     */
    public function login()
    {
        $email = $_POST['email'] ?? '';
        $password = $_POST['password'] ?? '';

        $user = Users::getUserForLogin($email, $password);

        if ($user && A::login_user($user)) {
            Response::json([
                'message' => 'Login was successful.'
            ]);
            Response::reroute('index');
        } else {
            Response::json([
                'message' => 'Wrong Credentials.'
            ], 403);
            Response::back();
        }
    }

    /**
     * logout User
     *
     * @return void
     * @throws Exception
     */
    public function logout()
    {

        if (A::logout_user()) {
            Response::json([
                'message' => 'Logout was successful.'
            ]);
        } else {
            Response::json([
                'message' => 'Something went wrong.'
            ], 500);
        }

        Response::reroute('index');
    }
}