<?php

namespace App\Controllers;

use Core\Controller;
use Core\Response;
use Core\Auth;

use App\Repositories\Users;
use Exception;

class Home extends Controller
{

    /**
     * Show the index page
     *
     * @return void
     * @throws Exception
     */
    public function index()
    {
        Response::render('home/index.php');
    }

    /**
     * Show the about page
     *
     * @return void
     * @throws Exception
     */
    public function about()
    {

        Response::render('home/about.php');
    }

    /**
     * Show the contact page
     *
     * @return void
     * @throws Exception
     */
    public function contact()
    {
        Response::render('home/contact.php');
    }

    /**
     * Show the about login
     *
     * @return void
     * @throws Exception
     */
    public function login()
    {
        Response::render('home/login.php');
    }
}
