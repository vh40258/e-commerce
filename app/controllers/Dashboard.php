<?php

namespace App\Controllers;

use Core\Controller;
use Core\Response;
use Core\Auth;

use App\Repositories\Users;
use App\Repositories\Products;
use Exception;

class Dashboard extends Controller
{

    public function __construct($route_params)
    {
        parent::__construct($route_params);
        if (Auth::user() === False || !Auth::user()->hasRole(['admin'])) {
            Response::reroute('products/index');
        }
    }

    /**
     * Show the index page
     *
     * @return void
     * @throws Exception
     */
    public function users()
    {
        $users = Users::getAll();
        Response::render('dashboard/users.php', [
            'users' => $users,
        ]);
    }

    /**
     * Show the index page
     *
     * @return void
     * @throws Exception
     */
    public function edit_user(int $id)
    {
        $user = Users::get($id);

        if (!$user) {
            Response::reroute('dashboard');
        }
        Response::render('dashboard/edit_user.php', [
            'user' => $user
        ]);
    }

    /**
     * Show the index page
     *
     * @return void
     * @throws Exception
     */
    public function create_user()
    {
        Response::render('dashboard/create_user.php');
    }

    /**
     * Show the index page
     *
     * @return void
     * @throws Exception
     */
    public function products()
    {
        $products = \App\Repositories\Products::getAll();
        Response::render('dashboard/products.php', [
            'products' => $products,
        ]);
    }

    /**
     * Show the index page
     *
     * @return void
     * @throws Exception
     */
    public function edit_product(int $id)
    {
        $product = Products::get($id);

        if (!$product) {
            Response::reroute('dashboard');
        }
        Response::render('dashboard/edit_product.php', [
            'product' => $product
        ]);
    }

    /**
     * Show the index page
     *
     * @return void
     * @throws Exception
     */
    public function create_product()
    {
        Response::render('dashboard/create_product.php');
    }
}
