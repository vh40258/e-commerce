<?php


namespace App\controllers;


use App\Repositories\Users;
use App\Repositories\Products as P;
use App\Models\Product;
use Core\Auth;
use Core\Controller;
use Core\Response;
use Exception;

class Products extends Controller
{
    /**
     * Show the index page
     *
     * @return void
     * @throws Exception
     */
    public function index()
    {
        $products = \App\Repositories\Products::getAll();
        Response::render('products/index.php', [
            'products' => $products,
        ]);
    }

    /**
     * Show the details page
     *
     * @return void
     * @throws Exception
     */
    public function details($id)
    {
        $product = \App\Repositories\Products::get($id);

        if (!$product) {
            Response::reroute('index');
        }
        Response::render('products/details.php', [
            'product' => $product
        ]);

    }


    /**
     * Create a product
     *
     * @return void
     * @throws Exception
     */
    public function create()
    {

        if (Auth::user() === False || !Auth::user()->hasRole(['admin'])) {
            Response::back();
        }

        $product = new Product();
        $product->name         = $_POST['name'] ?? '';
        $product->description  = $_POST['description'] ?? '';
        $product->user_id      = $_POST['user_id'] ?? '';
        $product->image        = $_POST['image'] ?? '';

        $new_product = P::create($product);



        if($product) {
            Response::json([
                'message' => 'Product was create successfully.'
            ]);
        }else{
            Response::json([
                'message' => 'Something went wrong.'
            ]);
        }

        Response::back();
    }

    /**
     * Create a product
     *
     * @return void
     * @throws Exception
     */
    public function edit($id)
    {

        if (Auth::user() === False || !Auth::user()->hasRole(['admin'])) {
            Response::back();
        }

        $product = new Product();
        $product->id            = $id;
        $product->name          = $_POST['name'] ?? '';
        $product->description   = $_POST['description'] ?? '';
        $product->user_id       = $_POST['user_id'] ?? '';
        $product->image         = $_POST['image'] ?? '';

        $edited = P::edit($product);

        if($edited) {
            Response::json([
                'message' => 'Product was create successfully.'
            ]);
        }else{
            Response::json([
                'message' => 'Something went wrong.'
            ]);
        }

        Response::back();
    }
}