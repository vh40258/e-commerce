<?php


namespace App\controllers;


use App\Models\Product;
use App\Models\User;
use App\Repositories\Products as P;
use App\Repositories\Users as U;
use Core\Auth;
use Core\Controller;
use Core\Response;

class Users extends Controller
{
    public function edit($id)
    {

        if (Auth::user() === False || !Auth::user()->hasRole(['admin'])) {
            Response::back();
        }

        $user = new User();
        $user->id            = $id;
        $user->first_name   = $_POST['first_name'] ?? '';
        $user->last_name    = $_POST['last_name'] ?? '';
        $user->email        = $_POST['email'] ?? '';
        $user->password     = $_POST['password'] ?? NULL;

        $edited = U::edit($user);

        if($edited) {
            Response::json([
                'message' => 'Product was create successfully.'
            ]);
        }else{
            Response::json([
                'message' => 'Something went wrong.'
            ]);
        }

        Response::back();
    }
}