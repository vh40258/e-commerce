<?php

Use Core\Response;

?>

<?php
Response::render("includes/header.php");
?>

    <section id="products">
        <div class="products">
            <?php foreach ($products as $product): ?>
                <div class="product">
                    <div class="image">
                        <img src="<?php echo asset('imgs/' . $product->image); ?>">
                    </div>
                    <h2>
                        <a href="/products/details/<?php echo $product->id; ?> ">
                            <?php echo $product->name; ?>
                        </a>
                    </h2>
                    <p>
                        <?php echo $product->description; ?>
                    </p>
                </div>
            <?php endforeach; ?>
        </div>
    </section>
<?php
Response::render("includes/footer.php");
?>