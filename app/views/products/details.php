<?php

Use Core\Response;

?>

<?php
Response::render("includes/header.php");
?>
    <section id="product-details">
        <div class="title">
            <h1><?php echo $product->name; ?></h1>
        </div>
        <div class="product-details">
            <div class="description-column">
                <div class="images">
                    <img src="<?php echo asset('imgs/' . $product->image); ?>">
                </div>
                <h3>Description</h3>
                <p>
                    <?php echo $product->description; ?>
                </p>
            </div>
            <div class="specifications-column">
                <ul class="specifications-list">
                    <li><span>User</span> <?php echo $product->user()->fullname(); ?> </li>
                </ul>
            </div>
        </div>
    </section>
<?php
Response::render("includes/footer.php");
?>