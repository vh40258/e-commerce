<?php

Use Core\Response;

/** @var TYPE_NAME $users */
?>

<?php
Response::render("includes/header.php");
?>

<?php foreach ($users as $user): ?>
    <p><?php echo $user->fullname(); ?></p>
<?php endforeach; ?>

<?php
Response::render("includes/footer.php");
?>