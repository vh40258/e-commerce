<?php

Use Core\Response;

?>

<?php
Response::render("includes/header.php");
?>
    <link rel="stylesheet" href="<?php echo asset('css/login-style.css'); ?>">
    <div class="hero">
        <div class="form-box">
            <div class="button-box">
                <div id="btn"></div>
                <button id="login-btn" type="button" onclick="login()" class="toggle-btn">Log In</button>
                <button id="register-btn" type="button" onclick="register()" class="toggle-btn">Register</button>
            </div>

            <div class="social-icons">
                <img src="<?php echo asset('imgs/icons/fb.png'); ?>" alt="">
                <img src="<?php echo asset('imgs/icons/tw.png'); ?>" alt="">
                <img src="<?php echo asset('imgs/icons/gp.png'); ?>" alt="">
            </div>

            <form id="login" action="/login" method="post" class="input-group">
                <input name="email" type="email" class="input-field" placeholder="User Email">
                <input name="password" type="password" class="input-field" placeholder="Enter Password">
                <input name="remeber" type="checkbox" class="check-box"><span>Remember Password</span>
                <button type="submit" class="submit-btn">Log In</button>
            </form>

            <form id="register" action="/register" method="post" class="input-group">
                <input name="first_name" type="text" class="input-field" placeholder="Enter First Name">
                <input name="last_name" type="text" class="input-field" placeholder="Enter Last Name">
                <input name="email" type="email" class="input-field" placeholder="Enter Email">
                <input name="password" type="password" class="input-field" placeholder="Give a password">
                <button type="submit" class="submit-btn">Register</button>
            </form>
        </div>
    </div>
    <script src="<?php echo asset('js/login.js'); ?>"></script>
<?php
Response::render("includes/footer.php");
?>