<?php

Use Core\Response;

?>

<?php
Response::render("includes/header.php");
?>

    <section id="newsletter">
        <div class="container">
            <h1>Subscribe to our newsletter </h1>
            <form action="../../../public/index.php" action="../../../public/index.php">
                <input type="email" name="email" placeholder="Email">
                <input type="submit" class="button1">
            </form>
        </div>
    </section>

    <section id="main">
        <div class="container">
            <article id="main-col">
                <h3 id="main-title">About Us</h3>
                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                    labore
                    et dolore magna aliquyam erat, et dolore magna aliquyam erat, et dolore magna aliquyam erat, et
                    dolore magna aliquyam erat, et dolore magna aliquyam erat, et dolore magna aliquyam erat,
                    et dolore magna aliquyam erat, et dolore magna aliquyam erat, et dolore magna aliquyam erat, et
                    dolore magna aliquyam erat,.</p>
                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                    labore
                    et dolore magna aliquyam erat, et dolore magna aliquyam erat, et dolore magna aliquyam erat, et
                    dolore magna aliquyam erat, et dolore magna aliquyam erat, et dolore magna aliquyam erat,
                    et dolore magna aliquyam erat, et dolore magna aliquyam erat, et dolore magna aliquyam erat, et
                    dolore magna aliquyam erat,.</p>
            </article>

            <aside id="sidebar">
                <div class="dark">
                    <h3>What we do</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt
                        ut
                        labore
                        et dolore magna aliquyam erat, et dolore magna aliquyam erat, et dolore magna aliquyam erat, et
                        dolore magna aliquyam erat, et dolore magna aliquyam erat, et dolore magna aliquyam erat,
                        et dolore magna aliquyam erat, et dolore magna aliquyam erat, et dolore magna aliquyam erat, et
                        dolore magna aliquyam erat,.</p>
                </div>

            </aside>
        </div>
    </section>

<?php
Response::render("includes/footer.php");
?>