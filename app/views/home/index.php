<?php

Use Core\Response;

?>

<?php
Response::render("includes/header.php");
?>

    <section id="showcase">
        <div class="container">
            <h1>Welcome to the Ecommerce</h1>
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore
                et dolore magna aliquyam erat,.</p>
        </div>
    </section>

    <section id="newsletter">
        <div class="container">
            <h1>Subscribe to our newsletter </h1>
            <form action="index.php" action="index.php">
                <input type="email" name="email" placeholder="Email">
                <input type="submit" class="button1">
            </form>
        </div>
    </section>

    <section id="boxes">
        <div class="container">
            <div class="box">
                <img src="<?php echo asset('imgs/laptops.png'); ?>">
                <h2>Laptops</h2>
                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr,Lorem ipsum dolor sit amet, consetetur
                    sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore
                    et dolore magna aliquyam erat, ut</p>
            </div>
            <div class="box">
                <img src="<?php echo asset('imgs/pc2.png'); ?>">
                <h2>PC</h2>
                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr,Lorem ipsum dolor sit amet, consetetur
                    sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore
                    et dolore magna aliquyam erat, ut</p>
            </div>
            <div class="box">
                <img src="<?php echo asset('imgs/headphones2.png'); ?>">
                <h2>Headphones</h2>
                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr,Lorem ipsum dolor sit amet, consetetur
                    sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore
                    et dolore magna aliquyam erat, ut</p>
            </div>
        </div>
    </section>

<?php
Response::render("includes/footer.php");
?>