<?php

Use Core\Response;

?>

<?php
Response::render("includes/header.php");
?>


    <section id="newsletter">
        <div class="container">
            <h1>Subscribe to our newsletter </h1>
            <form action="../../../public/index.php" action="../../../public/index.php">
                <input type="email" name="email" placeholder="Email">
                <input type="submit" class="button1">
            </form>
        </div>
    </section>

    <section id="main">
        <div class="container">
            <article id="services-col">
                <h3 id="services-title">Our Services</h3>
                <ul>
                    <li>
                        <h3>Whater service here</h3>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                            invidunt ut
                            labore
                            et dolore magna aliquyam erat, et dolore magna aliquyam erat, et dolore magna aliquyam erat,
                            et
                            dolore magna aliquyam erat, et dolore magna aliquyam erat, et dolore magna aliquyam erat,
                            et dolore magna aliquyam erat, et dolore magna aliquyam erat, et dolore magna aliquyam erat,
                            et
                            dolore magna aliquyam erat,.</p>
                        <p>Pricing : 20$ - 40$</p>
                    </li>

                    <li>
                        <h3>Another service here</h3>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                            invidunt ut
                            labore
                            et dolore magna aliquyam erat, et dolore magna aliquyam erat, et dolore magna aliquyam erat,
                            et
                            dolore magna aliquyam erat, et dolore magna aliquyam erat, et dolore magna aliquyam erat,
                            et dolore magna aliquyam erat, et dolore magna aliquyam erat, et dolore magna aliquyam erat,
                            et
                            dolore magna aliquyam erat,.</p>
                        <p>Pricing : 20$ - 40$</p>
                    </li>
                </ul>

            </article>

            <aside id="sidebar">
                <div class="dark">
                    <h3>Contact Us</h3>
                    <form action="" method="post" class="contact-form">
                        <div>
                            <label for="name">Name</label>
                            <input type="text" placeholder="Name" name="username">
                        </div>
                        <div>
                            <label for="Email">Email</label>
                            <input type="email" placeholder="Email" name="email">
                        </div>
                        <div>
                            <label for="message">Message</label>
                            <textarea name="message" id="" cols="30" rows="10"></textarea>
                        </div>
                        <input type="submit" class="button1" name="Send">
                    </form>
                </div>

            </aside>
        </div>
    </section>

<?php
Response::render("includes/footer.php");
?>