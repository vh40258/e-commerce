<?php

Use Core\Response;

?>

<?php
Response::render("includes/dashboard_header.php");
?>

<form id="register" action="/register" method="post">
    <div class="form-group">
        <label for="exampleInputEmail1">First Name</label>
        <input name="first_name" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter First Name">
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Last Name</label>
        <input name="last_name" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Last Name">
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Email address">
        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
    </div>
    <div class="form-group">
        <label for="exampleFormControlTextarea1">Bio</label>
        <textarea name="bio" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
<?php
Response::render("includes/dashboard_footer.php");
?>


