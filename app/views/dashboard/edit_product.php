<?php

Use Core\Response;

?>

<?php
Response::render("includes/dashboard_header.php");
?>
<form id="register" action="/products/edit/<?php echo $product->id?>" method="post">
    <div class="form-group">
        <label for="exampleInputEmail1">Name</label>
        <input name="name" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Name" value="<?php echo $product->name; ?>">
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Description</label>
        <input name="description" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Description" value="<?php echo $product->description; ?>">
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">User ID</label>
        <input name="user_id" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter User ID" value="<?php echo $product->user_id; ?>">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Image</label>
        <input name="image" type="text" class="form-control" id="exampleInputPassword1" placeholder="Image" value="<?php echo $product->image; ?>">
    </div>
    <button type="submit" class="btn btn-primary">Edit</button>
</form>
<?php
Response::render("includes/dashboard_footer.php");
?>


