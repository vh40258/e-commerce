<?php

Use Core\Response;

?>

<?php
Response::render("includes/dashboard_header.php");
?>
<link rel="stylesheet" href="<?php echo asset('css/table.css'); ?>">

<a href="/dashboard/create_product">Create Product</a>

<div class="wrap-table100">
    <div class="table100 ver1 m-b-110">
        <div class="table100-head">
            <table>
                <thead>
                <tr class="row100 head">
                    <th class="cell100 column1">ID</th>
                    <th class="cell100 column2">Name</th>
                    <th class="cell100 column3">Description</th>
                    <th class="cell100 column4">User ID</th>
                    <th class="cell100 column5">Image Path</th>
                    <th class="cell100 column6">Edit</th>
                </tr>
                </thead>
            </table>
        </div>

        <div class="table100-body js-pscroll">
            <table>
                <tbody>
                <?php foreach ($products as $product): ?>
                    <tr class="row100 body">
                        <td class="cell100 column1"><?php echo $product->id; ?> </td>
                        <td class="cell100 column2"><?php echo $product->name; ?></td>
                        <td class="cell100 column3"><?php echo $product->description; ?></td>
                        <td class="cell100 column4"><?php echo $product->user_id; ?></td>
                        <td class="cell100 column5"><?php echo $product->image; ?></td>
                        <td class="cell100 column6"><a href="/dashboard/product/<?php echo $product->id; ?>">Edit</a></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php
Response::render("includes/dashboard_footer.php");
?>


