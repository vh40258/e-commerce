<?php

Use Core\Response;

?>

<?php
Response::render("includes/dashboard_header.php");
?>
<link rel="stylesheet" href="<?php echo asset('css/table.css'); ?>">
<a href="/dashboard/create_user">Create User</a>


<div class="wrap-table100">
    <div class="table100 ver1 m-b-110">
        <div class="table100-head">
            <table>
                <thead>
                <tr class="row100 head">
                    <th class="cell100 column1">ID</th>
                    <th class="cell100 column2">First Name</th>
                    <th class="cell100 column3">Last Name</th>
                    <th class="cell100 column4">Email</th>
                    <th class="cell100 column5">Bio</th>
                    <th class="cell100 column6">Edit</th>
                </tr>
                </thead>
            </table>
        </div>

        <div class="table100-body js-pscroll">
            <table>
                <tbody>
                <?php foreach ($users as $user): ?>
                    <tr class="row100 body">
                        <td class="cell100 column1"><?php echo $user->id; ?> </td>
                        <td class="cell100 column2"><?php echo $user->first_name; ?></td>
                        <td class="cell100 column3"><?php echo $user->last_name; ?></td>
                        <td class="cell100 column4"><?php echo $user->email; ?></td>
                        <td class="cell100 column5"><?php echo $user->bio; ?></td>
                        <td class="cell100 column6"><a href="/dashboard/user/<?php echo $user->id; ?>">Edit</a></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php
Response::render("includes/dashboard_footer.php");
?>


