<?php

use Core\Auth;

$user = Auth::user();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ecommerce</title>
    <link rel="stylesheet" href="<?php echo asset('css/style.css'); ?>">
    <script src="<?php echo asset('js/jquery-3.5.1.min.js'); ?>"></script>
</head>

<body>
<header>
    <div class="container">
        <div id="brand">
            <a href="/index.php">
                <h1>SHOPBOTIC</h1>
                <p>
                    <?php if ($user) {
                        echo $user->fullname();
                    } ?>
                </p>
            </a>
        </div>

        <nav>
            <ul>
                <li><a href="/index">HOME</a></li>
                <li><a href="/products/index">PRODUCTS</a></li>
                <li><a href="/index">SHOP</a></li>
                <li><a href="/about">ABOUT</a></li>
                <li><a href="/contact">CONTACT US</a></li>
                <?php if (Auth::user()): ?>
                    <li>
                        <form action="/logout" method="POST" enctype="multipart/form-data">
                            <button type="submit">LOGOUT</button>
                        </form>
                    </li>
                    <?php if (Auth::user()->hasRole(['admin'])): ?>
                        <li><a href="/dashboard">Dashboard</a></li>
                    <?php endif; ?>
                <?php else: ?>
                    <li><a href="/login">LOGIN</a></li>
                <?php endif; ?>
                <li>
                    <form method="get" action="index.php" enctype="multipart/form-data">
                        <input type="text" name="user_query" placeholder="Search a product"/>
                        <input type="submit" name="search" value="Search"/>
                    </form>
                </li>
            </ul>
            </ul>
        </nav>
    </div>
</header>