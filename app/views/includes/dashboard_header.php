<?php

use Core\Auth;

$user = Auth::user();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js"
            integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe"
            crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo asset('css/dashboard.css'); ?>">
    <title>Admin Dashboard</title>
</head>
<body>

<!-- navbar -->
<nav class="navbar navbar-expand-md navbar-light">
    <button class="navbar-toggler ml-auto mb-2 bg-light" type="button" data-toggle="collapse" data-target="#myNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="myNavbar">
        <div class="container-fluid">
            <div class="row">
                <!-- sidebar -->
                <div class="col-xl-2 col-lg-3 col-md-4 sidebar fixed-top">
                    <a href="/index"
                       class="navbar-brand text-white d-block mx-auto text-cendashboard.phpter py-3 mb-4 bottom-border">E-Commerce</a>
                    <div class="bottom-border pb-3">
                        <img src="<?php echo asset('imgs/baba.png'); ?>" width="50" class="rounded-circle mr-3">
                        <a href="#" class="text-white"><?php echo $user->fullname();?></a>
                    </div>
                    <ul class="navbar-nav flex-column mt-4">
                        <li class="nav-item"><a href="/dashboard/users"
                                                class="nav-link text-white p-3 mb-2 sidebar-link current"><i
                                        class="fas fa-user text-light fa-lg mr-3"></i>Users</a></li>
                        <li class="nav-item"><a href="/dashboard/products"
                                                class="nav-link text-white p-3 mb-2 sidebar-link current"><i
                                        class="fas fa-shopping-cart text-light fa-lg mr-3"></i>Products</a></li>
                    </ul>
                </div>
                <!-- end of sidebar -->

                <!-- top-nav -->
                <div class="col-xl-10 col-lg-9 col-md-8 ml-auto bg-dark fixed-top py-2 top-navbar">
                    <div class="row align-items-center">
                        <div class="col-md-4">
                            <h4 class="text-light text-uppercase mb-0">Dashboard</h4>
                        </div>
<!--                        <div class="col-md-5">-->
<!--                            <form>-->
<!--                                <div class="input-group">-->
<!--                                    <input type="text" class="form-control search-input" placeholder="Search...">-->
<!--                                    <button type="button" class="btn btn-white search-button"><i-->
<!--                                                class="fas fa-search text-danger"></i></button>-->
<!--                                </div>-->
<!--                            </form>-->
<!--                        </div>-->
                    </div>
                </div>
                <!-- end of top-nav -->
            </div>
        </div>
    </div>
</nav>

<div class="col-xl-10 col-lg-9 col-md-8 ml-auto pt-5">

<!-- end of navbar -->