-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.13-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping data for table ecommerce.products: ~3 rows (approximately)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `name`, `description`, `user_id`, `image`) VALUES
	(1, 'Llaptopghghfgh', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit incidunt ipsum nihil nostrum nulla.', 1, 'laptops.png'),
	(2, 'Computer', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit incidunt ipsum nihil nostrum nulla.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit incidunt ipsum nihil nostrum nulla.Lorem ipsum dolor sit amet, consectetur adipisicing elit.', 1, 'laptops.png'),
	(3, 'Rri n\'Shpi', 'sdfsdf', 1, 'sdfsdf');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Dumping data for table ecommerce.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`) VALUES
	(1, 'admin'),
	(2, 'user');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping data for table ecommerce.roles_users: ~1 rows (approximately)
/*!40000 ALTER TABLE `roles_users` DISABLE KEYS */;
INSERT INTO `roles_users` (`id`, `user_id`, `role_id`) VALUES
	(1, 17, 1);
/*!40000 ALTER TABLE `roles_users` ENABLE KEYS */;

-- Dumping data for table ecommerce.users: ~5 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `bio`) VALUES
	(1, 'Admin', 'Admin', 'admin@ubt-uni.net', '$2y$10$R2krr343ZpvQGLb6QjKBoOndT5L/7O706hFEEQysmhTggR4PMfAh.', '')
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
