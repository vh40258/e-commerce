<?php

function asset(String $asset)
{
    $protocol = $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http");
    $port = $_SERVER['SERVER_PORT'];

    if ($asset[0] === '/') {
        $asset = ltrim($asset, '/');
    }

    return $protocol . '://' . $_SERVER['SERVER_NAME'] . ':' . $port . '/' . $asset;
}
