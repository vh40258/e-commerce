<?php

namespace Core;

use Exception;

/**
 * Response
 *
 * PHP version 7.0
 */
class Response
{

    /**
     * Render a view file
     *
     * @param string $view The view file
     * @param array $args Associative array of data to display in the view (optional)
     *
     * @return void
     * @throws Exception
     */
    public static function render($view, $args = [])
    {
        extract($args, EXTR_SKIP);

        $file = dirname(__DIR__) . "/App/Views/$view";  // relative to Core directory

        if (is_readable($file)) {
            require $file;
        } else {
            throw new Exception("$file not found");
        }
    }

    /**
     * Render a view file
     *
     * @param array $args
     * @param int $code
     * @return void
     * @throws Exception
     */
    public static function json($args, $code = 200)
    {
        if (is_array($args)) {
            header('Content-Type: application/json');
            http_response_code($code);
            echo json_encode($args);
        } else {
            throw new Exception("Response should be array!");
        }
    }

    /**
     * Render a view file
     *
     * @param array $args
     * @param int $code
     * @return void
     * @throws Exception
     */
    public static function reroute($url)
    {
        header("Location: " . asset($url));
    }

    public static function back()
    {
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }
}
