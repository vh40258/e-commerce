<?php


namespace Core;


use App\Models\User;
use App\Repositories\Users;

class Auth
{
    protected static $user = Null;

    public static function user()
    {
        if (isset($user)) {
            return $user;
        }

        if (isset($_SESSION['user'])) {
            return Users::get($_SESSION['user']);
        }

        $user = false;
        return $user;
    }

    public static function login_user(User $user)
    {
        try {
            $_SESSION["user"] = $user->id;
            return True;
        } catch (\Exception $e) {
            return False;
        }
    }

    public static function logout_user()
    {
        try {
            unset($_SESSION["user"]);
            return True;
        } catch (\Exception $e) {
            return False;
        }
    }
}