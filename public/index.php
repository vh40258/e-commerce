<?php

/**
 * Front controller
 *
 * PHP version 7.0
 */

use Core\Response;

/**
 * Composer
 */
require dirname(__DIR__) . '/vendor/autoload.php';

require dirname(__DIR__) . '/core/helpers.php';

/**
 * Error and Exception handling
 */
error_reporting(E_ALL);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');


/**
 * Routing
 */

$router = new Core\Router();

// Add the routes


// Auth
$router->add('login', [
    'controller' => 'auth',
    'action' => 'login',
    'verb'  =>  'POST'
]);
$router->add('logout', [
    'controller' => 'auth',
    'action' => 'logout',
    'verb'  =>  'POST'
]);
$router->add('register', [
    'controller' => 'auth',
    'action' => 'register',
    'verb'  =>  'POST'
]);
$router->add('register', [
    'controller' => 'auth',
    'action' => 'register',
    'verb'  =>  'POST'
]);


//Dashboard
$router->add('dashboard', ['controller' => 'dashboard', 'action' => 'users']);
$router->add('dashboard/user/{id:\d+}', ['controller' => 'dashboard', 'action' => 'edit_user']);
$router->add('dashboard/product/{id:\d+}', ['controller' => 'dashboard', 'action' => 'edit_product']);
$router->add('dashboard/create_user', ['controller' => 'dashboard', 'action' => 'create_user']);
$router->add('dashboard/create_product', ['controller' => 'dashboard', 'action' => 'create_product']);
$router->add('users/edit/{id:\d+}', ['controller' => 'Users', 'action' => 'edit', 'verb'  =>  'POST']);

// Default Routes
$router->add('', ['controller' => 'home', 'action' => 'index']);
$router->add('{action}', ['controller' => 'home']);
$router->add('{controller}/{action}');

//Products
$router->add('products/details/{id:\d+}', ['controller' => 'Products', 'action' => 'details']);
$router->add('product/create', [
    'controller' => 'Products',
    'action' => 'create',
    'verb'  =>  'POST'
]);
$router->add('products/edit/{id:\d+}', ['controller' => 'Products', 'action' => 'edit', 'verb'  =>  'POST']);
session_start();
$router->dispatch($_SERVER['QUERY_STRING']);
