var login_form = document.getElementById("login");
var register_form = document.getElementById("register")
var login_form_submit_button = login_form.querySelector('button[type=submit]');
var register_form_submit_button = register_form.querySelector('button[type=submit]');

var z = document.getElementById("btn");

function register() {
    login_form.style.left = "-400px";
    register_form.style.left = "50px";
    z.style.left = "110px";
}

function login() {
    login_form.style.left = "50px";
    register_form.style.left = "450px";
    z.style.left = "0px";
}

login_form_submit_button.addEventListener("click", function (event) {
    if (!isValid(login_form)) {
        event.preventDefault();
    }
});
register_form_submit_button.addEventListener("click", function (event) {
    if (!isValid(register_form)) {
        event.preventDefault();
    }
});

function isValid(form) {
    let validateText = /^[a-z]{2,}$/i;
    let emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let inputs = form.children
    let hasError = false;

    for (let item of inputs) {
        if (item.tagName == 'INPUT') {
            switch (item.getAttribute("type")) {
                case 'text':
                    if (validateText.test(item.value)) {
                        item.classList.remove("error");
                    } else {
                        item.classList.add("error");
                        hasError = true;
                    }
                    break;
                case 'email':
                    if (emailRegex.test(item.value)) {
                        item.classList.remove("error");
                    } else {
                        item.classList.add("error");
                        hasError = true;
                    }
                    break;
                case 'password':
                    if (item.value.length >= 8) {
                        item.classList.remove("error");
                    } else {
                        item.classList.add("error");
                        hasError = true;
                    }
                    break;
            }
        }
    }

    return !hasError;
}